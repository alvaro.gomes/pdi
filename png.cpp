#include <stdlib.h>
#include <png.h>

#include "png.h"

namespace png {
	void abort_(const char * s, ...) {
		abort();
	}

	pdi::image<pdi::pixel> read_file (std::string file) {
		unsigned char header[8];    // 8 is the maximum size that can be checked

		/* open file and test for it being a png */
		FILE *fp = fopen(file.c_str(), "rb");
		if (!fp)
			abort_("[read_png_file] File %s could not be opened for reading", file.c_str());
		fread(header, 1, 8, fp);
		if (png_sig_cmp(header, 0, 8))
			abort_("[read_png_file] File %s is not recognized as a PNG file", file.c_str());

		/* initialize stuff */
		png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

		if (!png_ptr)
			abort_("[read_png_file] png_create_read_struct failed");

		png_infop info_ptr = png_create_info_struct(png_ptr);
		if (!info_ptr)
			abort_("[read_png_file] png_create_info_struct failed");

		if (setjmp(png_jmpbuf(png_ptr)))
			abort_("[read_png_file] Error during init_io");

		png_init_io(png_ptr, fp);
		png_set_sig_bytes(png_ptr, 8);

		png_read_info(png_ptr, info_ptr);

		int width = png_get_image_width(png_ptr, info_ptr);
		int height = png_get_image_height(png_ptr, info_ptr);
//		int color_type = png_get_color_type(png_ptr, info_ptr);
//		int bit_depth = png_get_bit_depth(png_ptr, info_ptr);

//		int number_of_passes = png_set_interlace_handling(png_ptr);
		png_read_update_info(png_ptr, info_ptr);

		/* read file */
		if (setjmp(png_jmpbuf(png_ptr)))
			abort_("[read_png_file] Error during read_image");

		png_bytep *row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
		int i, j; // Assumindo grayscale
		for (i=0; i<height; i++)
			row_pointers[i] = (png_byte*) malloc(png_get_rowbytes(png_ptr,info_ptr));

		png_read_image(png_ptr, row_pointers);
		fclose(fp);

		pdi::image<pdi::pixel> img(width, height);
		for (j=0; j<height; j++) {
			png_byte* row = row_pointers[j];
			for (i=0; i<width; i++) {
				img.color(i, j, row[i]);
			}
		}

		/* cleanup heap allocation */
		for (i=0; i<height; i++)
			free(row_pointers[i]);
		free(row_pointers);

		return img;
	}

	void write_file (std::string file, const pdi::image<pdi::pixel> &img) {
		/* create file */
		FILE *fp = fopen(file.c_str(), "wb");
		if (!fp)
			abort_("[write_png_file] File %s could not be opened for writing", file.c_str());

		/* initialize stuff */
		png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

		if (!png_ptr)
			abort_("[write_png_file] png_create_write_struct failed");

		png_infop info_ptr = png_create_info_struct(png_ptr);
		if (!info_ptr)
			abort_("[write_png_file] png_create_info_struct failed");

		if (setjmp(png_jmpbuf(png_ptr)))
			abort_("[write_png_file] Error during init_io");

		png_init_io(png_ptr, fp);

		/* write header */
		if (setjmp(png_jmpbuf(png_ptr)))
			abort_("[write_png_file] Error during writing header");

		// Assumindo grayscale
		png_set_IHDR(png_ptr, info_ptr, img.width(), img.height(),
			8, PNG_COLOR_TYPE_GRAY, PNG_INTERLACE_NONE,
			PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

		png_write_info(png_ptr, info_ptr);

		/* write bytes */
		if (setjmp(png_jmpbuf(png_ptr)))
			abort_("[write_png_file] Error during writing bytes");

		png_bytep *row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * img.height());
		int i;
		for (i=0; i<img.height(); i++)
			row_pointers[i] = (png_byte*) malloc(img.width());

		for (int i=0; i<img.width(); i++) {
			for (int j=0; j<img.height(); j++) { // Assumindo grayscale
				row_pointers[j][i] = img.color(i, j).red;
			}
		}

		png_write_image(png_ptr, row_pointers);

		/* end write */
		if (setjmp(png_jmpbuf(png_ptr)))
			abort_("[write_png_file] Error during end of write");

		png_write_end(png_ptr, NULL);

		/* cleanup heap allocation */
		for (i=0; i<img.height(); i++)
			free(row_pointers[i]);
		free(row_pointers);

		fclose(fp);
	}
}
