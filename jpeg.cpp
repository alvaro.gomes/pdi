#include <stdio.h>
#include <jpeglib.h>

#include "jpeg.h"

namespace jpg {
	pdi::image<pdi::pixel> read_file (std::string file) {
		struct jpeg_decompress_struct cinfo;
		struct jpeg_error_mgr jerr;
		cinfo.err = jpeg_std_error (&jerr);
		jpeg_create_decompress (&cinfo);

		FILE *in = fopen (file.c_str (), "rb");
		if (in == NULL) {
			jpeg_destroy_decompress (&cinfo);
			throw file; // Isso precisa ser melhorado
		}
		jpeg_stdio_src (&cinfo, in);

		jpeg_read_header (&cinfo, true);
		jpeg_start_decompress (&cinfo);

		pdi::image<pdi::pixel> ret(cinfo.image_width, cinfo.image_height);
		int i, row_stride = cinfo.image_width * cinfo.output_components;
		unsigned char *row = new unsigned char[row_stride];
		while (cinfo.output_scanline < cinfo.image_height) {
			jpeg_read_scanlines (&cinfo, &row, 1);
			for (i=0; i<ret.width(); i++) { // Assumindo grayscale
				ret.color(i, cinfo.output_scanline, row[i]);
			}
		}

		jpeg_finish_decompress (&cinfo);
		fclose (in);
		jpeg_destroy_decompress (&cinfo);

		return ret;
	}

	void write_file (std::string file, const pdi::image<pdi::pixel> &image) {
		struct jpeg_compress_struct cinfo;
		struct jpeg_error_mgr jerr;
		cinfo.err = jpeg_std_error (&jerr);
		jpeg_create_compress (&cinfo);

		FILE *out = fopen (file.c_str (), "wb");
		if (out == NULL) {
			jpeg_destroy_compress (&cinfo);
			return;
		}
		jpeg_stdio_dest (&cinfo, out);

		cinfo.image_width = image.width();
		cinfo.image_height = image.height();
		cinfo.input_components = 1; // Assumindo Grayscale
		cinfo.in_color_space = JCS_GRAYSCALE;

		jpeg_set_defaults (&cinfo);
		jpeg_set_quality (&cinfo, 100, true);
		jpeg_start_compress (&cinfo, true);

		int i, row_stride = image.width() * cinfo.input_components;
		unsigned char *row = new unsigned char[row_stride];
		while (cinfo.next_scanline < cinfo.image_height) {
			for (i=0; i<image.width(); i++) { // Assumindo grayscale
				row[i] = image.color(i, cinfo.next_scanline).red;
			}
			jpeg_write_scanlines (&cinfo, &row, 1);
		}

		jpeg_finish_compress (&cinfo);
		fclose (out);
		jpeg_destroy_compress (&cinfo);
	}
}
