#include <string>

#include "pdi.hpp"

#ifndef _PNG_H_
#define _PNG_H_

namespace png {
	pdi::image<pdi::pixel> read_file (std::string file);
	void write_file (std::string file, const pdi::image<pdi::pixel> &img);
}

#endif
