#include <cstddef>

#ifndef _PDI_H_
#define _PDI_H_

namespace pdi {
	struct pixel {
		unsigned char red;
		unsigned char green;
		unsigned char blue;

		pixel (unsigned char r = 255, unsigned char g = -1,
				unsigned char b = -1);

		pixel operator+ (const pixel &p) const;
		pixel operator- (const pixel &p) const;

		bool operator< (const pixel &p) const;
		bool operator> (const pixel &p) const;
	};

	struct complex {
		double real;
		double imag;

		complex (const double r = 0, const double i = 0);

		complex operator+ (const complex& val) const;
		complex operator- (const complex& val) const;
	};

	template <typename T>
	class image {
		int width_;
		int height_;
		T **color_;

		public:
		image (int w, int h, T *c = NULL) {
			width_ = w;
			height_ = h;

			color_ = new T*[width_];
			int i, j;
			for (i=0; i<width_; i++) {
				color_[i] = new T[height_];
				if (c != NULL) {
					for (j=0; j<height_; j++) {
						color_[i][j] = c[j * width_ + i];
					}
				}
			}
		}
		image (const image& img) {
			width_ = img.width_;
			height_ = img.height_;

			color_ = new T*[width_];
			int i, j;
			for (i=0; i<width_; i++) {
				color_[i] = new T[height_];
				for (j=0; j<height_; j++) {
					color_[i][j] = img.color_[i][j];
				}
			}
		}
		~image () {
			int i;
			for (i=0; i<width_; i++) {
				delete[] color_[i];
			}
			delete[] color_;
		}

		image operator+ (const image &img) {
			if (width_ != img.width_ || height_ != img.height_) {
				return img;
			}

			image n(width_, height_);
			int i, j;
			for (i=0; i<width_; i++) {
				for (j=0; j<height_; j++) {
					n.color_[i][j] = color_[i][j] + img.color_[i][j];
				}
			}

			return n;
		}
		image operator- (const image &img) {
			if (width_ != img.width_ || height_ != img.height_) {
				return img;
			}

			image n(width_, height_);
			int i, j;
			for (i=0; i<width_; i++) {
				for (j=0; j<height_; j++) {
					n.color_[i][j] = color_[i][j] - img.color_[i][j];
				}
			}

			return n;
		}

		int width () const {
			return width_;
		}
		int height () const {
			return height_;
		}
		T color (int x, int y) const {
			return color_[x][y];
		}
		void color (int x, int y, T color) {
			color_[x][y] = color;
		}
		T* column (int x) const {
			int i;
			T *ret = new T[height_];
			for (i=0; i<height_; i++) {
				ret[i] = color_[x][i];
			}
			return ret;
		}
		T* row (int y) const {
			int i;
			T *ret = new T[width_];
			for (i=0; i<width_; i++) {
				ret[i] = color_[i][y];
			}
			return ret;
		}
	};

	image<pixel> logarithm (const image<pixel> &img);
	image<pixel> gamma (double gamma, const image<pixel> &img);
	int* histogram (const image<pixel> &img);
	image<pixel> equalize_histogram (const image<pixel> &img);

	image<pixel> filter (const image<pixel> &img, const image<double> &window);
	image<pixel> filter_median (int width, int height, const image<pixel> &img);
	image<pixel> undot (const image<pixel> &img);

	image<pixel> pepper (double prob, const image<pixel> &img);
	image<pixel> salt_pepper (double prob, const image<pixel> &img);
	image<pixel> gauss_noise (int med, int dev, const image<pixel> &img);

	image<pixel> threshold (int threshold, const image<pixel> &img);
	image<pixel> local_threshold (int size, const image<pixel> &img);
	image<pixel> k_means (int k, const image<pixel> &img);

	image<complex> dft2d (const image<pixel> &img);
	image<pixel> idft2d (const image<complex> &img);
//------------------
//	image<complex> group (const image<complex> &img);

	image<complex> low_pass (const image<complex> &img, int range = 20);
}

#endif
