#include <string>

#include "pdi.hpp"

#ifndef _JPEG_H_
#define _JPEG_H_

namespace jpg {
	pdi::image<pdi::pixel> read_file (std::string file);
	void write_file (std::string file, const pdi::image<pdi::pixel> &image);
}

#endif
