.PHONY: clean

CXXFLAGS=-g -Wall -std=c++11
LDFLAGS=-ljpeg -lpng

pdi: pdi.o jpeg.o png.o q1.cpp
	g++ $(CXXFLAGS) pdi.o jpeg.o png.o q1.cpp $(LDFLAGS) -o pdi

pdi.o: pdi.h pdi.cpp
	g++ $(CXXFLAGS) -c pdi.cpp -o pdi.o

jpeg.o: pdi.h jpeg.h jpeg.cpp
	g++ $(CXXFLAGS) -c jpeg.cpp -o jpeg.o

png.o: pdi.h png.h png.cpp
	g++ $(CXXFLAGS) -c png.cpp -o png.o

clean:
	rm *.o q1
