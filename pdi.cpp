#include <algorithm>
#include <cmath>
#include <vector>
#include <random>

#include "pdi.h"

namespace pdi {
	pixel::pixel (unsigned char r, unsigned char g, unsigned char b) {
		red = r;

		if (g < 0) {
			green = r;
		} else {
			green = g;
		}

		if (b < 0) {
			blue = r;
		} else {
			blue = b;
		}
	}

	pixel pixel::operator+ (const pixel &p) const {
		pixel n;

		int aux = red + p.red;
		if (aux > 255) {
			n.red = 255;
		} else {
			n.red = aux;
		}

		aux = green + p.green;
		if (aux > 255) {
			n.green = 255;
		} else {
			n.green = aux;
		}

		aux = blue + p.blue;
		if (aux > 255) {
			n.blue = 255;
		} else {
			n.blue = aux;
		}

		return n;
	}

	pixel pixel::operator- (const pixel &p) const {
		pixel n;

		int aux = red - p.red;
		if (aux < 0) {
			n.red = 0;
		} else {
			n.red = aux;
		}

		aux = green - p.green;
		if (aux < 0) {
			n.green = 0;
		} else {
			n.green = aux;
		}

		aux = blue - p.blue;
		if (aux < 0) {
			n.blue = 0;
		} else {
			n.blue = aux;
		}

		return n;
	}

	bool pixel::operator< (const pixel &p) const {
		int bright = red + green + blue; // Talvez nao seja a melhor forma
		return bright < p.red + p.green + p.blue;
	}

	bool pixel::operator> (const pixel &p) const {
		int bright = red + green + blue; // Talvez nao seja a melhor forma
		return bright > p.red + p.green + p.blue;
	}

	complex::complex (const double r, const double i) {
		real = r;
		imag = i;
	}

	complex complex::operator+ (const complex& val) const {
		return complex(real + val.real, imag + val.imag);
	}

	complex complex::operator- (const complex& val) const {
		return complex(real - val.real, imag - val.imag);
	}

	image<pixel> logarithm (const image<pixel> &img) {
		int i, j;
		double max = 0;
		double **log_img = new double*[img.width()];
		for (i=0; i<img.width(); i++) {
			log_img[i] = new double[img.height()];
			for (j=0; j<img.height(); j++) { // Assumindo Grayscale
				log_img[i][j] = log (img.color(i, j).red + 1);
				if (log_img[i][j] > max) {
					max = log_img[i][j];
				}
			}
		}

		image<pixel> ret(img.width(), img.height());
		max = 255 / max;
		for (i=0; i<img.width(); i++) {
			for (j=0; j<img.height(); j++) {
				ret.color(i, j, log_img[i][j] * max);
			}
		}

		for (i=0; i<img.width(); i++) {
			delete[] log_img[i];
		}
		delete[] log_img;

		return ret;
	}

	image<pixel> gamma (double gamma, const image<pixel> &img) {
		int i, j;
		double max = 0;
		double **gam_img = new double*[img.width()];
		for (i=0; i<img.width(); i++) {
			gam_img[i] = new double[img.height()];
			for (j=0; j<img.height(); j++) { // Assumindo Grayscale
				gam_img[i][j] = pow (img.color(i, j).red, gamma);
				if (gam_img[i][j] > max) {
					max = gam_img[i][j];
				}
			}
		}

		image<pixel> ret(img.width(), img.height());
		max = 255 / max;
		for (i=0; i<img.width(); i++) {
			for (j=0; j<img.height(); j++) {
				ret.color(i, j, gam_img[i][j] * max);
			}
		}

		for (i=0; i<img.width(); i++) {
			delete[] gam_img[i];
		}
		delete[] gam_img;

		return ret;
	}

	int* histogram (const image<pixel> &img) {
		int *hist = new int[256];
		int i, j;
		for (i=0; i<256; i++) {
			hist[i] = 0;
		}

		for (i=0; i<img.width(); i++) { // Assumindo grayscale
			for (j=0; j<img.height(); j++) {
				hist[img.color(i, j).red] ++;
			}
		}
		return hist;
	}

	image<pixel> equalize_histogram (const image<pixel> &img) {
		int *hist = histogram(img);

		int i, j;
		for (i=1; i<256; i++) {
			hist[i] += hist[i-1];
		}

		for (i=0; i<256; i++) {
			hist[i] = hist[i] * 255 / hist[255];
		}

		image<pixel> ret (img.width(), img.height());
		for (i=0; i<img.width(); i++) { // Assumindo grayscale
			for (j=0; j<img.height(); j++) {
				ret.color(i, j, hist[img.color(i, j).red]);
			}
		}
		return ret;
	}

	pixel apply_window (int x, int y, const image<double> &win, const image<pixel> &img) {
		int i, j, sum = 0;
		double div = 0;
		for (i=0; i<win.width(); i++) {
			for (j=0; j<win.height(); j++) {
				int posX = x + i - win.width()/2;
				int posY = y + j - win.height()/2;
				if (posX > 0 && posX < img.width() && posY > 0
						&& posY < img.height()) { // Assumindo Grayscale
					sum += win.color(i, j) * img.color(posX, posY).red;
					div += win.color(i, j);
				}
			}
		}

		if (div != 0) {
			return sum / div;
		} else {
			sum = sum > 255 ? 255 : sum;
			return sum < 0 ? 0 : sum;
		}
	}

	image<pixel> filter (const image<pixel> &img, const image<double> &window) {
		image<pixel> n(img);

		int i, j;
		for (i=0; i<n.width(); i++) {
			for (j=0; j<n.height(); j++) {
				n.color(i, j, apply_window (i, j, window, img));
			}
		}

		return n;
	}

	pixel calculate_median (int x, int y, int width,
			int height, const image<pixel> &img) {
		std::vector<pixel> ordered;

		int i, j;
		for (i=0; i<width; i++) {
			for (j=0; j<height; j++) {
				int posX = x + i - width/2;
				int posY = y + j - height/2;
				if (posX > 0 && posX < img.width() && posY > 0
						&& posY < img.height()) {
					ordered.push_back(img.color(posX, posY));
				}
			}
		}

		std::sort(ordered.begin(), ordered.end());
		return ordered[ordered.size()/2];
	}

	image<pixel> filter_median (int width, int height, const image<pixel> &img) {
		image<pixel> ret(img.width(), img.height());
		int i, j;
		for (i=0; i<img.width(); i++) {
			for (j=0; j<img.height(); j++) {
				ret.color(i, j, calculate_median (i, j, width, height, img));
			}
		}
		return ret;
	}

	pixel max_neighbor (int x, int y, const image<pixel> &img) {
		pixel ret(0);
		int i, j;
		for (i=x-1; i<=x+1; i++) {
			for (j=y-1; j<=y+1; j++) {
				if (i > 0 && i < img.width() && y > 0 && y < img.height()
						&& (i != x || j != y)) {
					if (img.color(i, j) > ret) {
						ret = img.color(i, j);
					}
				}
			}
		}
		return ret;
	}

	pixel min_neighbor (int x, int y, const image<pixel> &img) {
		pixel ret(255);
		int i, j;
		for (i=x-1; i<=x+1; i++) {
			for (j=y-1; j<=y+1; j++) {
				if (i > 0 && i < img.width() && y > 0 && y < img.height()
						&& (i != x || j != y)) {
					if (img.color(i, j) < ret) {
						ret = img.color(i, j);
					}
				}
			}
		}
		return ret;
	}

	image<pixel> undot (const image<pixel> &img) {
		int i, j;
		image<pixel> ret(img.width(), img.height());
		for (i=0; i<img.width(); i++) {
			for (j=0; j<img.height(); j++) {
				if (img.color(i, j) < min_neighbor(i, j, img)) {
					ret.color(i, j, min_neighbor(i, j, img));
				} else if (img.color(i, j) > max_neighbor(i, j, img)) {
					ret.color(i, j, max_neighbor(i, j, img));
				} else {
					ret.color(i, j, img.color(i, j));
				}
			}
		}
		return ret;
	}

	image<pixel> pepper (double prob, const image<pixel> &img) {
		std::default_random_engine de(0);
		std::uniform_real_distribution<double> rd(0.0, 1.0);

		int i, j;
		image<pixel> ret(img.width(), img.height());
		for (i=0; i<img.width(); i++) {
			for (j=0; j<img.height(); j++) {
				if (prob > rd(de)) {
					ret.color(i, j, 0);
				} else {
					ret.color(i, j, img.color(i, j));
				}
			}
		}

		return ret;
	}

	image<pixel> salt_pepper (double prob, const image<pixel> &img) {
		std::default_random_engine de(0);
		std::uniform_real_distribution<double> rd(0.0, 1.0);

		int i, j;
		image<pixel> ret(img.width(), img.height());
		for (i=0; i<img.width(); i++) {
			for (j=0; j<img.height(); j++) {
				if (prob > rd(de)) {
					if (0.5 > rd(de)) {
						ret.color(i, j, 0);
					} else {
						ret.color(i, j, 255);
					}
				} else {
					ret.color(i, j, img.color(i, j));
				}
			}
		}

		return ret;
	}

	image<pixel> gauss_noise (int med, int dev, const image<pixel> &img) {
		std::default_random_engine de;
		std::normal_distribution<double> bd(med, dev);

		int i, j;
		image<pixel> ret(img.width(), img.height());
		for (i=0; i<img.width(); i++) {
			for (j=0; j<img.height(); j++) { // Assumindo Grayscale
				ret.color(i, j, img.color(i, j).red + bd(de));
			}
		}

		return ret;
	}

	image<pixel> threshold (int threshold, const image<pixel> &img) {
		image<pixel> ret(img.width(), img.height());

		int i, j;
		for (i=0; i<img.width(); i++) {
			for (j=0; j<img.height(); j++) {
				if (img.color(i, j) < threshold) {
					ret.color(i, j, 0);
				} else {
					ret.color(i, j, 255);
				}
			}
		}

		return ret;
	}

	image<pixel> local_threshold (int size, const image<pixel> &img) {
		image<pixel> ret(img.width(), img.height());
		double med;
		int i, j, k, l;
		for (i=0; i<img.width()/size; i++) {
			for (j=0; j<img.height()/size; j++) {
				med = 0;
				for (k=0; k<size; k++) {
					for (l=0; l<size; l++) { // Assumindo Grayscale
						med += img.color(i*size+k, j*size+l).red;
					}
				}
				med /= (size * size);
				for (k=0; k<size; k++) {
					for (l=0; l<size; l++) { // Assumindo Grayscale
						if (img.color(i*size+k, j*size+l).red > med) {
							ret.color(i*size+k, j*size+l, 255);
						} else {
							ret.color(i*size+k, j*size+l, 0);
						}
					}
				}
			}
		}
		return ret;
	}

	image<pixel> k_means (int k, const image<pixel> &img) {
		image<pixel> ret(img.width(), img.height());
		double aux1, means[k], means_old[k], sums[k], stride = 255 / k;

		int i, j, l, aux2, divs[k];
		for (i=0; i<k; i++) {
			means[i] = i * stride;
			sums[i] = 0;
			divs[i] = 0;
		}

		while (stride > 1) {
			for (i=0; i<img.width(); i++) {
				for (j=0; j<img.height(); j++) { // Assumindo Grayscale
					aux1 = std::abs(img.color(i, j).red - means[0]);
					aux2 = 0;
					for (l=1; l<k; l++) {
						if (aux1 > std::abs(img.color(i, j).red - means[l])) {
							aux1 = std::abs(img.color(i, j).red - means[l]);
							aux2 = l;
						}
					}
					ret.color(i, j, 255 * aux2 / (k-1));
					sums[aux2] += img.color(i, j).red;
					divs[aux2] ++;
				}
			}

			stride = 0;
			for (i=0; i<k; i++) {
				means_old[i] = means[i];
				means[i] = sums[i] / divs[i];
				stride += std::abs(means_old[i] - means[i]);
			}
		}

		return ret;
	}

	complex* dft (pixel *data, int size) {
		complex *ret = new complex[size];

		long i, k;
		double arg, cosarg, sinarg;

		for (i=0; i<size; i++) {
			ret[i].real = 0;
			ret[i].imag = 0;

			arg = - 2.0 * M_PI * (double)i / (double)size;
			for (k=0; k<size; k++) {
				cosarg = cos(k * arg);
				sinarg = sin(k * arg);
				ret[i].real += (data[k].red * cosarg);
				ret[i].imag += (data[k].red * sinarg);
			}
		}

		return ret;
	}

	void dft (complex *data, int size) {
		long i, k;
		double arg, cosarg, sinarg;

		complex temp[size];
		for (i=0; i<size; i++) {
			temp[i].real = 0;
			temp[i].imag = 0;

			arg = - 2.0 * M_PI * (double)i / (double)size;
			for (k=0; k<size; k++) {
				cosarg = cos(k * arg);
				sinarg = sin(k * arg);
				temp[i].real += (data[k].real * cosarg - data[k].imag * sinarg);
				temp[i].imag += (data[k].real * sinarg + data[k].imag * cosarg);
			}
		}

		for (i=0; i<size; i++) {
			data[i].real = temp[i].real;
			data[i].imag = temp[i].imag;
		}
	}

	void idft1 (complex *data, int size) {
		long i, k;
		double arg, cosarg, sinarg;

		complex temp[size];
		for (i=0; i<size; i++) {
			temp[i].real = 0;
			temp[i].imag = 0;

			arg = - 2.0 * M_PI * (double)i / (double)size;
			for (k=0; k<size; k++) {
				cosarg = cos(k * arg);
				sinarg = sin(k * arg);
				temp[i].real += (data[k].real * cosarg - data[k].imag * sinarg);
				temp[i].imag += (data[k].real * sinarg + data[k].imag * cosarg);
			}
		}

		for (i=0; i<size; i++) {
			data[i].real = temp[size-i].real / (double)size;
			data[i].imag = temp[size-i].imag / (double)size;
		}
	}

	pixel* idft (complex *data, int size) {
		pixel *ret = new pixel[size];

		long i, k;
		double arg, cosarg, sinarg;

		double temp[size];
		for (i=0; i<size; i++) {
			temp[i] = 0;

			arg = - 2.0 * M_PI * (double)i / (double)size;
			for (k=0; k<size; k++) {
				cosarg = cos(k * arg);
				sinarg = sin(k * arg);
				temp[i] += (data[k].real * cosarg - data[k].imag * sinarg);
			}
		}

		for (i=0; i<size; i++) {
			ret[i] = temp[size-i] / (double)size;
		}

		return ret;
	}

	image<complex> dft2d (const image<pixel> &img) {
		image<complex> ret(img.width(), img.height());
		pixel *t1;
		complex *t2;

		int i, j;
		// Transform the rows
		for (j=0; j<img.height(); j++) {
			t1 = img.row(j);
			t2 = dft(t1, img.width());
			for (i=0; i<img.width(); i++) {
				ret.color(i, j, t2[i]);
			}
			delete[] t1;
			delete[] t2;
		}

		// Transform the columns
		for (j=0; j<img.width(); j++) {
			t2 = ret.column(j);
			dft(t2, img.height());
			for (i=0; i<img.height(); i++) {
				ret.color(j, i, t2[i]);
			}
			delete[] t2;
		}

		return ret;
	}

	image<pixel> idft2d (const image<complex> &img) {
		image<complex> img2(img);
		image<pixel> ret(img.width(), img.height());
		complex *t1;
		pixel *t2;

		int i, j;
		// Transform the rows
		for (j=0; j<img.height(); j++) {
			t1 = img.row(j);
			idft1(t1, img.width());
			for (i=0; i<img.width(); i++) {
				img2.color(i, j, t1[i]);
			}
			delete[] t1;
		}

		// Transform the columns
		for (j=0; j<img.width(); j++) {
			t1 = img2.column(j);
			t2 = idft(t1, img.height());
			for (i=0; i<img.height(); i++) {
				ret.color(j, i, t2[i]);
			}
			delete[] t2;
		}

		return ret;
	}
}
