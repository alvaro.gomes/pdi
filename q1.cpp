#include <iostream>
#include "jpeg.h"

int main (int argc, char **argv) {
	double h[9] = {1, 1, 1,
					0, 0, 0,
					-1, -1, -1};
	double v[9] = {1, 0, -1,
					1, 0, -1,
					1, 0, -1};
	double d1[9] = {0, 1, 1,
					-1, 0, 1,
					-1, -1, 0};
	double d2[9] = {1, 1, 0,
					1, 0, -1,
					0, -1, -1};

	pdi::image<double> h_filter(3, 3, h);
	pdi::image<double> v_filter(3, 3, v);
	pdi::image<double> d1_filter(3, 3, d1);
	pdi::image<double> d2_filter(3, 3, d2);

	pdi::image<pdi::pixel> img = jpg::read_file ("../Imagens/1.jpg");
	pdi::image<pdi::pixel> h_line = pdi::filter (img, h_filter);
	pdi::image<pdi::pixel> v_line = pdi::filter (img, v_filter);
	pdi::image<pdi::pixel> d1_line = pdi::filter (img, d1_filter);
	pdi::image<pdi::pixel> d2_line = pdi::filter (img, d2_filter);
	pdi::image<pdi::pixel> line = h_line + v_line + d1_line + d2_line;
	jpg::write_file ("../Resultados/1-pre.jpg", line);
	pdi::image<pdi::pixel> out = pdi::threshold (80, line);
	jpg::write_file ("../Resultados/1.jpg", out);

	pdi::image<pdi::pixel> img2 = jpg::read_file ("../Imagens/2.jpg");
	pdi::image<pdi::pixel> means2 = pdi::k_means (3, img2);
	pdi::image<pdi::pixel> out2 = pdi::filter_median (5, 5, means2);
	jpg::write_file ("../Resultados/2.jpg", out2);

	pdi::image<pdi::pixel> img3 = jpg::read_file ("../Imagens/3.jpg");
	pdi::image<pdi::pixel> means3 = pdi::k_means (2, img3);
	jpg::write_file ("../Resultados/3.jpg", means3);

	pdi::image<pdi::pixel> img4 = jpg::read_file ("../Imagens/4.jpg");
	pdi::image<pdi::pixel> h_line4 = pdi::filter (img4, h_filter);
	pdi::image<pdi::pixel> v_line4 = pdi::filter (img4, v_filter);
	pdi::image<pdi::pixel> out4 = pdi::threshold (100, h_line4 + v_line4);
	jpg::write_file ("../Resultados/4.jpg", out4);

	return 0;
}
